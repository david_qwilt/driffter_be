import subprocess

from driffter.chart_museum import getLatestChartVersions


def getServicesVersions(namespaceNeeded):
    bashCmd = ["helm", "ls", "-n", str(namespaceNeeded)]
    process = subprocess.Popen(bashCmd, stdout=subprocess.PIPE)

    output, error = process.communicate()
    out = output.decode("utf-8")
    listServices = out.replace(" ", "").split('\n')

    services_dict = {}
    for line in listServices[1:]:
        # print(line.split('\t'), "\n")
        line_list = line.split('\t')
        if len(line_list) < 7:
            continue
        # print(line_list)
        service_name = line_list[0]
        namespace = line_list[1]
        service = service_name.replace(namespace + "-", "")
        chart = line_list[5]
        current_version = chart.replace(service + "-", "")
        # print(service, current_version)
        services_dict[service] = current_version

    return services_dict


# print(getServicesVersions("yuvalk"))


def get_namespace_services_dict(namespace, application_type):
    services_dict = getServicesVersions(namespace)
    latestChartVersions = getLatestChartVersions(application_type)
    # jenkinsLink =
    namespace_services_dict = {}
    for service in services_dict:
        version = services_dict.get(service)
        latest_version = ""
        if service in latestChartVersions:
            latest_version = latestChartVersions.get(service)
            needToDrift = False
            if version != latest_version:
                needToDrift = True
            namespace_services_dict[service] = {"current_version": version, "latest_version": latest_version,
                                                "needToDrift": needToDrift}
        else:
            namespace_services_dict[service] = {"current_version": version, "latest_version": latest_version,
                                                "needToDrift": False}
    return namespace_services_dict


# print(get_namespace_services_dict("yuvalk", "dev"))
