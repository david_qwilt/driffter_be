from typing import Dict, Union
import pdb


def add_jenkins_api_call_to_data_dict(
    data_dict: Dict[str, Dict[str, Union[str, bool]]], namespace: str
) -> Dict[str, Dict[str, Union[str, bool]]]:
    for key in data_dict.keys():
        if data_dict[key]["needToDrift"]:
            data_dict[key]["jenkins_url"] = add_jenkins_url(
                data_dict[key], namespace, key
            )

    return data_dict


def add_jenkins_url(service_dict, namespace, ms_name) -> str:
    ms = ms_name
    ms_base_chart = get_base_chart_form_ms(ms_name)
    latest_version = service_dict["latest_version"]
    return f"https://jenkins.op.qwilt.com/job/devops-dev-shared-deploy-microservices/buildWithParameters?&account=qwilt-cq-rnd&region=eu-west-1&ENV=dev&MS={ms}&MS_BASE_CHART={ms_base_chart}&newStructureImplementation=true&version={latest_version}&namespace={namespace}&blue_green_enabled=true&dryRun=false&runTerraform=true&db_migrate=true&checkSanity=false&infra_branch=master"


def get_base_chart_form_ms(ms_name: str) -> Union[str, None]:
    if "-qwos" in ms_name:
        return ms_name.split("-qwos")[0]
    elif "-BT" in ms_name:
        return ms_name.split("-BT")[0]

    return ms_name
