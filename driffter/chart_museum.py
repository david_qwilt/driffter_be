import requests
import dateutil.parser


def getLatestChartVersions(appType):
    response = requests.get("https://chartmuseum.rnd.cqloud.com/api/charts")
    response_body_json = response.json()
    latest = {}
    for ms in response_body_json.values():
        versions = []
        if appType in ['GA', 'DT']:
            versions = [(x['name'], x['version'], dateutil.parser.isoparse(x['created']))
                 for x in ms if x['version'].endswith(appType)]
        else:
            versions = [(x['name'], x['version'], dateutil.parser.isoparse(x['created']))
                 for x in ms if not x['version'].endswith('GA') and not x['version'].endswith('DT')]
        if len(versions) > 0:
            versions.sort(key=lambda x: x[2], reverse=True)
            latest[versions[0][0]] = versions[0][1]
    return latest


getLatestChartVersions('DT')