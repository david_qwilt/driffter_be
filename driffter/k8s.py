from kubernetes import client, config

def get_namespaces_list():
    namespaces_list = []
    # Configs can be set in Configuration class directly or using helper utility
    config.load_kube_config()

    v1 = client.CoreV1Api()
    print("Listing pods with their IPs:")
    ret2 = v1.list_namespace(watch=False)
    for i in ret2.items:
        # print("%s" % i.metadata.name)
        namespaces_list.append(i.metadata.name)
    return namespaces_list
