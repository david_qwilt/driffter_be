from flask import Flask, request

from driffter.currentVersions import get_namespace_services_dict
from driffter.jenkins_controller import add_jenkins_api_call_to_data_dict
from driffter.k8s import get_namespaces_list
from flask import jsonify

app = Flask(__name__)


@app.route("/")
def index():
    return "Hello it is the Driffter app"


@app.route("/namespaces", methods=["GET"])
def all_namespaces():
    """
    return the list of all available namespaces
    """
    return jsonify(get_namespaces_list())

# curl http://127.0.0.1:5000/namespaces/yuvalk/drifts?application_type=dev/dt/ga
@app.route("/namespaces/<name_space>/drifts", methods=["GET"])
def namespace_drifts(name_space):
    """
    return the namespace with a list of versions/drifts (data structure should match frontend for ease of use)
    """
    application_type = request.args.get('application_type').upper()
    dict = get_namespace_services_dict(name_space,application_type)
    data_dict = add_jenkins_api_call_to_data_dict(dict,name_space)
    return jsonify(data_dict)



@app.route("/namespaces/<name_space>/sync", methods=["POST"])
def sync(name_space):
    """
    /namespace/:ns/sync?app_name=<>&version=<> - should currently return a a jenkins jobs link with all relevant fields / run jenkins job

    Params for jenkins
    -------------------

    """
    pass


@app.route("/namespaces/<name_space>/refresh", methods=["POST"])
def refresh_all_now(name_space):
    """
    /namespaces/:ns/refresh - ask for refresh right now, go fetch data for specific ns and update the result on the fly

    """
    pass


@app.route("/namespaces/<name_space>/wakeup", methods=["POST"])
def wakeup_namespace(name_space):
    """
    /namespaces/:ns/wakeup - nice to have, wakeup my namespace if it’s currently down
    """
    pass
